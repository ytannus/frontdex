## Frontdex Overview

&nbsp;
### Introduction

Frontdex frontend application is a user interface component for interaction with other parts of OpenDAX-v4 decentralized exchange.

It is a React based application with all necessary tools for your wallets and orders management, it provides the connection of UI and actions on Finex and goTrue backend.

&nbsp;
### Technologies

To build Frontdex application we use next technologies and libraries:

- [**Next.js**](https://nextjs.org/)

  Next.js is a React framework for developing single page Javascript applications. The benefit of this framework is server-side rendering alongside with speed and performance boost.

- [**TypeScript**](https://www.typescriptlang.org/)

  TypeSCript is a strongly typed programming language that builds on JavaScript, giving you better tooling at any scale. TypeScript developed and maintained by Microsoft.

- [**web3-react**](https://github.com/NoahZinsmeister/web3-react)

  web3-react is a simple, powerful framework for building modern Ethereum dApps using React. It supports commonly used web3 providers, including MetaMask/Trust/Tokenary, Infura/QuikNode, Trezor/Ledger, WalletConnect, Fortmatic/Portis, and more.

- [**Ethers**](https://github.com/ethers-io/ethers.js)

  Ethers library is complete Ethereum wallet implementation and utilities in JavaScript (and TypeScript) which keep your private keys in your client, safe and sound


- [**Tailwind CSS**](https://tailwindcss.com/)

  A utility-first CSS framework packed with classes that can be composed to build any design, directly in your markup.

- [**Redux**](https://redux.js.org/)

  Redux is a predictable state container for JavaScript apps. It helps you write applications that behave consistently, run in different environments (client, server, and native), and are easy to test.

- [**React-use-websocket**](https://github.com/robtaussig/react-use-websocket)

  React Hook designed to provide robust WebSocket integrations to React Components.


- [**@supabse/gotrue-js**](https://github.com/supabase/gotrue-js)

  An isomorphic JavaScript client library for the GoTrue API. In Frontdex we use authentication with MetaMask integrated into the [GoTrue](https://github.com/netlify/gotrue) service.

- [**@openware/react-opendax**](https://www.npmjs.com/package/@openware/react-opendax)

  React Tailwind components for OpenDAX v4

&nbsp;
### Internal code structure

Here is Frontdex application internal code structure with the content description of the main folders:

```

- /app          # folder where Redux store configured an initialized containing 2 hooks - useDispatch and useSelector

- /components   # stores UI components with logic which not included in the lib @openware/react-opendax

- /configs      # folder where default configs for platform stored

- /contracts    # contain Smart Contract related settings. e.g. ABI json.

- /features     # stores Redux actions and trigger actions to change ReduxStore

- /gotrue       # folder where GoTrue client initialized and stores GoTrueAuthWrapper component which handles platform authorization

- /pages        # folder where stored code of the platform pages

- /webscokets   # place where WebSocketWrapper function stored which handle websocket interaction
```

&nbsp;
#### Pages overview

OpenDAX-v4 decentralized application UI has next pages:

##### Trading (/trading/{marketName})

The main page of the platform where you can find TradinView, Orderbook, user's balances, Open Orders and Create Order sections

<img src="./images/trading.png" alt="Trading page" height="450">


##### History (/history/deposits and /history/withdrawals)

This page consists of 2 different tabs - Deposit History and Withdraw History

<img src="./images/history.png" alt="History page" height="450">

##### Orders (/orders/all and /orders/open)

On this page, user can find the list of orders with all states or orders with `open` state on 2 different tabs

<img src="./images/orders.png" alt="Orders page" height="450">

##### Settings (/settings)

Page with settings where user can change color theme (dark or light), language or he can disconnect his wallet

<img src="./images/settings.png" alt="Settings page" height="450">


##### Balances (/balances, only for mobile view)

Page where user can find a balance of all his tokens. This page can be seen only from mobile devices since it's a part of `/trading` page on devices with higher resolution

<img src="./images/balances.png" alt="Balances page" height="550">

### Build & development process

#### Local development

Run the development server:

```bash
npm install && npm run dev
```

Open [http://localhost:3001](http://localhost:3001) with your browser to see the result.


You can start editing the page by modifying files in `pages/` folder. The page auto-updates as you edit the file.

&nbsp;
#### Application deployment

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.
