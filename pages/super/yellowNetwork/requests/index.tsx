import * as React from 'react';
import { useRouter } from 'next/router';
import { withAuth, withAdminAuth } from '@openware/opendax-web-sdk';

const Requests: React.FC = () => {
    const router = useRouter();

    React.useEffect(() => {
        router.replace('/super/yellowNetwork/requests/incoming');
    }, []);

    return null;
}

export default withAuth(withAdminAuth(Requests));
