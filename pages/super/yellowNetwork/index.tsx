import * as React from 'react';
import { useRouter } from 'next/router';
import { withAuth, withAdminAuth } from '@openware/opendax-web-sdk';

const YellowNetwork: React.FC = () => {
    const router = useRouter();

    React.useEffect(() => {
        router.replace('/super/yellowNetwork/brokers');
    }, []);

    return null;
}

export default withAuth(withAdminAuth(YellowNetwork));
