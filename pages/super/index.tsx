import * as React from 'react';
import { useRouter } from 'next/router';
import { withAuth, withAdminAuth } from '@openware/opendax-web-sdk';

const Super: React.FC = () => {
    const router = useRouter();

    React.useEffect(() => {
        router.replace('/super/activation');
    }, []);

    return null;
}

export default withAuth(withAdminAuth(Super));
