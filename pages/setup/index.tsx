import { appTitle, Wizard } from '@openware/opendax-web-sdk'
import Head from 'next/head'
import React from 'react'

const WizardPage: React.FC = (): JSX.Element => {
    return (
        <>
            <Head>
                <title>{appTitle('Wizard')}</title>
            </Head>
            <Wizard />
        </>
    )
}

export default WizardPage
